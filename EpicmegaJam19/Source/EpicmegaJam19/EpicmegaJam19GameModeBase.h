// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EpicmegaJam19GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EPICMEGAJAM19_API AEpicmegaJam19GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
